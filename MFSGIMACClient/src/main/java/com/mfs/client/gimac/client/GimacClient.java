package com.mfs.client.gimac.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class GimacClient {
	private static String baseUrl = "http://localhost:8080/MfsGimacWeb/";

	public static void main(String[] args) {

		GimacClient gimacClient = new GimacClient();

		String response = null;

		String[] jsonResponseParams = new String[3];

		String params = "";

		Map<String, Object> jsonResponse1 = new HashMap<String, Object>();

		Map<String, String> jsonResponse = new HashMap<String, String>();

		int choice = Integer.parseInt(args[0]);


		try {
			switch (choice) {
			case 1:
				response = gimacClient.authorization();
				jsonResponse = gimacClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("access_token")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;
			case 2:
				response = gimacClient.paymentInquiry(args[1], args[2], args[3], args[4], args[5], args[6], args[7],
						args[8], args[9], args[10], args[11], args[12], args[13]);
				jsonResponse1 = gimacClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("issuerTrxRef")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse1.toString();

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 3:
				response = gimacClient.accountIncomingRemittance(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18],args[19], args[20],args[21], args[22],args[23]);

				jsonResponse1 = gimacClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("issuerTrxRef")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.get("issuerTrxRef");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 4:
				response = gimacClient.remittanceCancellation(args[1], args[2], args[3], args[4], args[5]);

				jsonResponse = gimacClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("issuerTrxRef")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.get("issuerTrxRef");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 5:
				response = gimacClient.walletRemittance(args[1], args[2], args[3], args[4], args[5], args[6],
						args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16],
						args[17], args[18],args[19], args[20],args[21], args[22],args[23], args[24]);

				jsonResponse1 = gimacClient.jsonToMapObject1(response);

				if (jsonResponse1 != null && response.contains("issuerTrxRef")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = (String) jsonResponse1.get("issuerTrxRef");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			case 6:
				response = gimacClient.walletCancellation(args[1], args[2], args[3], args[4], args[5]);

				jsonResponse = gimacClient.jsonToMapObject(response);

				if (jsonResponse != null && response.contains("issuerTrxRef")) {
					jsonResponseParams[0] = "01";
					jsonResponseParams[1] = "Success";
					jsonResponseParams[2] = jsonResponse.get("issuerTrxRef");

					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));

				} else {
					jsonResponseParams[0] = "02";
					jsonResponseParams[1] = "Fail";
					params = Arrays.toString(jsonResponseParams);
					System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
				}
				break;

			}

		} catch (Exception e) {
			jsonResponseParams[0] = "02";
			jsonResponseParams[1] = "Fail";
			jsonResponseParams[2] = e.getMessage();
			params = Arrays.toString(jsonResponseParams);
			System.out.println(params.toString().replace("[", "").replace("]", "").replace(", ", ","));
		}

	}

	private String walletCancellation(String issuerTrxRef, String intent, String updateTime, String voucherCode,
			String state) throws Exception {
		String serviceUrl = baseUrl + "walletCancellation";
		JsonObject walletCancellation = new JsonObject();
		walletCancellation.addProperty("issuerTrxRef", issuerTrxRef);
		walletCancellation.addProperty("intent", intent);
		walletCancellation.addProperty("updateTime", updateTime);
		walletCancellation.addProperty("voucherCode", voucherCode);
		walletCancellation.addProperty("state", state);
		return getConnectionjsonResponse(serviceUrl, walletCancellation.toString());

	}

	private String walletRemittance(String issuerTrxRef, String intent, String createTime, String senderMobile,
			String walletDestination, String toMember, String description, String amount, String currency, String senderFirstName,
			String senderSecondName, String senderIdType, String senderIdNumber, String senderAddress, String senderBirthDate,
			String receiverFirstName, String receiverSecondName, String receiverIdType, String receiverIdNumber, String receiverAddress,
			String city,
			String country, String phone, String receiverBirthDate) throws Exception {
		String serviceUrl = baseUrl + "walletRemittance";
		JsonObject walletRemittance = new JsonObject();
		walletRemittance.addProperty("issuerTrxRef", issuerTrxRef);
		walletRemittance.addProperty("intent", intent);
		walletRemittance.addProperty("createTime", createTime);
		walletRemittance.addProperty("senderMobile", senderMobile);
		walletRemittance.addProperty("walletDestination", walletDestination);
		walletRemittance.addProperty("toMember", toMember);
		walletRemittance.addProperty("description", description);
		walletRemittance.addProperty("amount", amount);
		walletRemittance.addProperty("currency", currency);

		JsonObject senderCustomerData = new JsonObject();
		senderCustomerData.addProperty("firstName", senderFirstName);
		senderCustomerData.addProperty("secondName", senderSecondName);
		senderCustomerData.addProperty("idType", senderIdType);
		senderCustomerData.addProperty("idNumber", senderIdNumber);
		senderCustomerData.addProperty("address", senderAddress);
		senderCustomerData.addProperty("birthDate", senderBirthDate);

		JsonObject receiverCustomerData = new JsonObject();
		receiverCustomerData.addProperty("firstName", receiverFirstName);
		receiverCustomerData.addProperty("secondName", receiverSecondName);
		receiverCustomerData.addProperty("idType", receiverIdType);
		receiverCustomerData.addProperty("idNumber", receiverIdNumber);
		receiverCustomerData.addProperty("address", receiverAddress);
		receiverCustomerData.addProperty("city", city);
		receiverCustomerData.addProperty("country", country);
		receiverCustomerData.addProperty("phone", phone);
		receiverCustomerData.addProperty("birthDate", receiverBirthDate);
		
		walletRemittance.add("senderCustomerData", senderCustomerData);
		walletRemittance.add("receiverCustomerData", receiverCustomerData);

		return getConnectionjsonResponse(serviceUrl, walletRemittance.toString());

	}

	private String paymentInquiry(String asIssuer, String asAcquirer, String firstElement, String pageSize,
			String acquirerTrxRef, String toMember, String toDate, String fromMember, String fromDate,
			String issuerTrxRef, String voucherCode, String status, String intents) throws Exception {

		String serviceUrl = baseUrl + "paymentInquiry";
		JsonObject paymentInquiryServiceRequest = new JsonObject();
		paymentInquiryServiceRequest.addProperty("asIssuer", asIssuer);
		paymentInquiryServiceRequest.addProperty("asAcquirer", asAcquirer);
		paymentInquiryServiceRequest.addProperty("firstElement", firstElement);
		paymentInquiryServiceRequest.addProperty("pageSize", pageSize);
		paymentInquiryServiceRequest.addProperty("acquirerTrxRef", acquirerTrxRef);
		paymentInquiryServiceRequest.addProperty("toMember", toMember);
		paymentInquiryServiceRequest.addProperty("fromDate", fromDate);
		paymentInquiryServiceRequest.addProperty("toDate", toDate);
		paymentInquiryServiceRequest.addProperty("fromMember", fromMember);
		paymentInquiryServiceRequest.addProperty("issuerTrxRef", issuerTrxRef);
		paymentInquiryServiceRequest.addProperty("voucherCode", voucherCode);
		
		JsonArray statusList = new JsonArray();
		statusList.add(status);
		
		JsonArray intentsList = new JsonArray();
		intentsList.add(intents);
		
		paymentInquiryServiceRequest.add("status", statusList.getAsJsonArray());
		paymentInquiryServiceRequest.add("intents", intentsList.getAsJsonArray());
		return getConnectionjsonResponse(serviceUrl, paymentInquiryServiceRequest.toString());
	}

	private String accountIncomingRemittance(String issuerTrxRef, String intent, String createTime, String senderMobile,
			String accountNumber, String toMember, String description, String amount, String currency, String senderFirstName,
			String senderSecondName, String senderIdType, String senderAddress, String senderBirthDate,
			String receiverFirstName,
			String receiverSecondName, String receiverIdType, String receiverIdNumber, String receiverAddress,
			String city,
			String country, String phone, String receiverBirthDate) throws Exception {

		String serviceUrl = baseUrl + "accountRemittance";
		JsonObject accountIncomingRemittance = new JsonObject();
		
		
		accountIncomingRemittance.addProperty("issuerTrxRef", issuerTrxRef);
		accountIncomingRemittance.addProperty("intent", intent);
		accountIncomingRemittance.addProperty("createTime", createTime);
		accountIncomingRemittance.addProperty("senderMobile", senderMobile);
		accountIncomingRemittance.addProperty("accountNumber", accountNumber);
		accountIncomingRemittance.addProperty("toMember", toMember);
		accountIncomingRemittance.addProperty("description", description);
		accountIncomingRemittance.addProperty("amount", amount);
		accountIncomingRemittance.addProperty("currency", currency);

		JsonObject senderCustomerData = new JsonObject();
		senderCustomerData.addProperty("firstName", senderFirstName);
		senderCustomerData.addProperty("secondName", senderSecondName);
		senderCustomerData.addProperty("idType", senderIdType);
		senderCustomerData.addProperty("address", senderAddress);
		senderCustomerData.addProperty("birthDate", senderBirthDate);

		JsonObject receiverCustomerData = new JsonObject();
		receiverCustomerData.addProperty("firstName", receiverFirstName);
		receiverCustomerData.addProperty("secondName", receiverSecondName);
		receiverCustomerData.addProperty("idType", receiverIdType);
		receiverCustomerData.addProperty("idNumber", receiverIdNumber);
		receiverCustomerData.addProperty("address", receiverAddress);
		receiverCustomerData.addProperty("city", city);
		receiverCustomerData.addProperty("country", country);
		receiverCustomerData.addProperty("phone", phone);
		receiverCustomerData.addProperty("birthDate", receiverBirthDate);
		
		accountIncomingRemittance.add("senderCustomerData", senderCustomerData);
		accountIncomingRemittance.add("receiverCustomerData", receiverCustomerData);

		return getConnectionjsonResponse(serviceUrl, accountIncomingRemittance.toString());
	}

	private String remittanceCancellation(String issuerTrxRef, String intent, String updateTime, String voucherCode,
			String state) throws Exception {
		String serviceUrl = baseUrl + "remittanceCancelltion";
		JsonObject remittanceCancellation = new JsonObject();
		remittanceCancellation.addProperty("issuerTrxRef", issuerTrxRef);
		remittanceCancellation.addProperty("intent", intent);
		remittanceCancellation.addProperty("updateTime", updateTime);
		remittanceCancellation.addProperty("voucherCode", voucherCode);
		remittanceCancellation.addProperty("state", state);
		return getConnectionjsonResponse(serviceUrl, remittanceCancellation.toString());
	}

	private String authorization() throws Exception {
		String authorizationTokenServiceUrl = baseUrl + "auth?user_name=login&password=password";
		return getConnectionjsonResponseGet(authorizationTokenServiceUrl);
	}

	private String getConnectionjsonResponse(String serviceUrl, String request) throws Exception {

		HttpURLConnection connection = null;

		URL url = new URL(serviceUrl);

		connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod("POST");

		connection.setRequestProperty("Content-type", "application/json");
		connection.setRequestProperty("Accept", "application/json");
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setConnectTimeout(20000);

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
		wr.writeBytes(request);
		wr.close();

		InputStream is = connection.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader rd = new BufferedReader(isr);
		StringBuilder jsonResponse = new StringBuilder();
		String line;

		while ((line = rd.readLine()) != null) {
			jsonResponse.append(line);
			jsonResponse.append('\r');
		}

		rd.close();

		if (connection != null) {
			connection.disconnect();
		}

		return jsonResponse.toString();
	}

	private String getConnectionjsonResponseGet(String serviceUrl) throws Exception {

		HttpURLConnection connection = null;

		URL url = new URL(serviceUrl);
		connection = (HttpURLConnection) url.openConnection();

		connection.setRequestMethod("GET");

		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setConnectTimeout(20000);

		InputStream is = connection.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader rd = new BufferedReader(isr);
		StringBuilder jsonResponse = new StringBuilder();
		String line;

		while ((line = rd.readLine()) != null) {
			jsonResponse.append(line);
			jsonResponse.append('\r');
		}

		rd.close();

		if (connection != null) {
			connection.disconnect();
		}

		return jsonResponse.toString();
	}

	Map<String, String> jsonToMapObject(String jsonString) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Map<String, String> map = new HashMap<String, String>();

		map = mapper.readValue(jsonString, new TypeReference<Map<String, String>>() {
		});

		return map;
	}

	Map<String, Object> jsonToMapObject1(String jsonString) throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		Map<String, Object> myObjects = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {
		});

		myObjects = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {
		});

		return myObjects;
	}

}
